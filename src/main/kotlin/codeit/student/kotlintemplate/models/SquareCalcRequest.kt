package codeit.student.kotlintemplate.models

data class SquareCalcRequest(val input: Int)